//This is a function that adds two arguments
function add(x, y) {
    let sum = x + y;
    return sum;
}
console.log(add(2, 4));

//This is an add function that accepts any number of arguments
// function sum() {
//     let s = 0;
//     for (let i=0; i < arguments.length; i++) {
//         s += arguments[i];
//     }
//     return s;
// } 
// console.log(sum(2,4));

//This is a function that multiplies two arguments
function multiply(num1, num2) {
    let total = 0;
    for (let i = 1; i <= num2; i++) {
        total = add(total, num1);
    }
    return total;
}
console.log(multiply(6, 8));

//This is a function that raises a number to a power
function power(num1, num2) {
    let total = num1;
    for (let i = 1; i < num2; i++) {
        total = multiply(total, num1);
    }
    return total;
}
console.log(power(2, 8));

//This is a function that returns a factorial of an argument
function factorial(num1) {
    let total = 1;
    for (let i = 1; i < num1; i++) {
        total = multiply(total, add(i, 1));
    }
    return total;
}
console.log(factorial(4));

//This is a fibonaci function
function fibonaci(num1) {
    if (num1 === 1) {
        return 0
    }
    else if(num1 === 2){
        return 1
    }
    else {
        let prev = 1;
        let prevPrev = 0;
        for (let i = 3; i <= num1; i++) {
            total = add(prev, prevPrev);
            prevPrev = prev;
            prev = total;
        }
        return total;
    }
}
console.log(fibonaci(8));